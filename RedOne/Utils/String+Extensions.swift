//
//  String+Extension.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, tableName: "Localizable", bundle: Bundle.main, value: "", comment: "")
    }
    
    func localizedFormatted(arguments: CVarArg...) -> String {
        return String(format: localized, arguments: arguments)
    }
    
    var url: URL? {
        return URL(string: self)
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
