//
//  AssemblyManagerProtocol.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

protocol AssemblyManagerProtocol {
    static var container: Container { get }
    func resetScope(scope: ObjectScope, in container: Container)
}

final class AssemblyManager : AssemblyManagerProtocol {
    
    static let container: Container = SwinjectStoryboard.defaultContainer
    
    func resetScope(scope: ObjectScope, in container: Container) {
        container.resetObjectScope(scope)
    }
}

extension ObjectScope {
    // User specific scope for managing container scopes
    // static let userScope = ObjectScope(storageFactory: PermanentStorage.init)
}

