//
//  ApplicationRouterProtocol.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//
import UIKit

protocol ApplicationRouterDependentProtocol: class {
    var applicationRouter: ApplicationRouterProtocol! { get set }
}

protocol ApplicationRouterProtocol {
    func start()
    func route()
    
    func changeRootViewController(ofType type: ViewControllers)
    func viewController(ofType type: ViewControllers) -> UIViewController
}

enum ViewControllers {
    
    // Screens
    case main
}

enum Storyboards: String {
    case main = "Main"
}

extension Storyboards {
    static func all() -> [Storyboards] {
        return [
            main
        ]
    }
    
    var name: String {
        return rawValue
    }
}

