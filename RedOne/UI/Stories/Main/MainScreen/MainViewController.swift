//
//  MainViewController.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//
import UIKit

protocol MainViewModelProtocol { }

protocol MainDataProviderProtocol {
    func getData() -> MainViewModelProtocol
}

final class MainViewController: UIViewController /*, ApplicationRouterDependentProtocol*/ {
    
    // MARK: - Constants
    
    fileprivate struct Constants {
        // static let cellIdentifier: String = "CellId"
    }
    
    // MARK: - Properties
    
    /*private*/ var dataProvider: MainDataProviderProtocol!
    
    // var applicationRouter: ApplicationRouterProtocol!
    
    // MARK: - IBOutlets
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - IBActions
    
    // MARK: - Private methods
    
}
