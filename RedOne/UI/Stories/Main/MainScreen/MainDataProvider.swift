//
//  MainDataProvider.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//
import Foundation

final class MainDataProvider: MainDataProviderProtocol {
    
    func getData() -> MainViewModelProtocol {
        return MainViewModel()
    }
}
