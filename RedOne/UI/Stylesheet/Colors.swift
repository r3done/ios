//
//  Colors.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//
import Foundation
import Hue

struct Colors {
    
    static let primary = UIColor(hex: "#D0011B")
    static let secondary = UIColor(hex: "#909092")
    
    static let primaryText = black
    static let secondaryText = secondary
    
    static let primaryBackground = white
    static let secondaryBackground = UIColor(hex: "#111111")

    static let navigationBar = white
    static let customNavigationBar = black
    
    static let error =  primary

    static let white = UIColor.white
    static let black = UIColor.black
    static let clear = UIColor.clear
}
