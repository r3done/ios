//
//  DataProvidersAssembly.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//

import Swinject

final class DataProvidersAssembly: Assembly {
    
    func assemble(container: Container) {

        container.register(MainDataProviderProtocol.self) { resolver in
            return MainDataProvider()
        }
        
    }
    
}
