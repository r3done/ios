//
//  AppDelegate.swift
//  RedOne
//
//  Created by Daniel Tombor on 2017. 03. 27..
//  Copyright © 2017. Daniel Tombor. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        DIManager.shared.application(
            application, didFinishLaunchingWithOptions: launchOptions)
        
        // Load application window
        window = DIManager.resolve(service: UIWindow.self)
        DIManager.resolve(service: ApplicationRouterProtocol.self).start()
        
        // Setup appearances
        setupAppearances()
        
        return true
    }
    
    private func setupAppearances() {
        setupNavigationbarAppearance()
    }
    
    private func setupNavigationbarAppearance() {
        
//        let navigationBarAppearace = UINavigationBar.appearance()
//        navigationBarAppearace.tintColor = Colors.white
//        navigationBarAppearace.barTintColor = Colors.green
//        navigationBarAppearace.setBackgroundImage(#imageLiteral(resourceName: "navigationBarBackgorund"), for: .default)
//        navigationBarAppearace.titleTextAttributes = [
//            NSFontAttributeName: Fonts.sfDisplayBold(fontSize: .big),
//            NSForegroundColorAttributeName: Colors.white
//        ]
    
    }
}

